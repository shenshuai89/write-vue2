import { initMixin } from "./init";
import { renderMixin } from "./render";
import { lifecycleMixin } from "./lifecycle";
import { stateMixin } from "./state";
import { initGlobalAPI } from "./initGlobalAPI/index";
function Vue(options) {
  this._init(options);
}
initMixin(Vue);
renderMixin(Vue);
lifecycleMixin(Vue);
stateMixin(Vue);

// global API
initGlobalAPI(Vue);

// dom diff demo
// template => render => vnode
/* import {compileToFunction} from "./compiler/index";
import {createElm, patch} from "./vdom/patch"
let vm1 = new Vue({
  data: {
    name: "foo",
  },
});

let render1 = compileToFunction(`<div id='app' class='c1' style='background:red;'>
  <div style='background:red;' key="a">A</div>
  <div style='background:yellow;' key="b">B</div>
  <div style='background:blue;' key="c">C</div>
</div>`);
// 通过render 转化为虚拟节点
let vnode = render1.call(vm1);  // vnode虚拟节点
// console.log(vnode, "render1");

let el = createElm(vnode); // el真实dom
// console.log(el, "el");

document.body.appendChild(el);
let vm2 = new Vue({
  data: {
    name: "bar",
    age:2
  }
})
let render2 = compileToFunction(`<div id='abc' b='c2' style='color:green'>
  <div style='background:blue;' key="q">Q</div>
  <div style='background:red;' key="a">A</div>
  <div style='background:yellow;' key="f">F</div>
  <div style='background:blue;' key="c">C</div>
  <div style='background:blue;' key="n">N</div>
</div>`);
let vnode2 = render2.call(vm2); // vnode

setTimeout(() => {
  patch(vnode, vnode2);
}, 1000) */
// diff只进行平级比对






export default Vue;
