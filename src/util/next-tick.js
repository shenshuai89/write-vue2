let callbacks = [];
let waiting = false;
export function nextTick(cb) {
  //   setTimeout(cb, 0);
  // 多次调用nextTick 如果没有刷新的时候 ，先把它放到数组中，等下次同时进行更新
  callbacks.push(cb);
  if (waiting === false) {
    setTimeout(() => {
      callbacks.forEach((cb) => cb());
      waiting = false;
      callbacks = [];
    }, 0);
    waiting = true;
  }
}
