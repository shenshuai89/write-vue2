// 重写能改变原数组的方法 push pop shift unshift reverse sort splice
let oldArrayMethods = Array.prototype;
export let arrayMethods = Object.create(oldArrayMethods);

const methods = [
  "push",
  "pop",
  "shift",
  "unshift",
  "reverse",
  "splice",
  "sort",
];

methods.forEach((method) => {
  arrayMethods[method] = function (...args) {
    const res = oldArrayMethods[method].apply(this, args);
    let inserted; // 表示新增插入的数据
    let ob = this.__ob__;
    switch (method) {
      case "push":
      case "unshift":
        inserted = args;
        break;
      case "splice": // 3个参数，splice(1,3, {name: "splice"}),第3个参数是新增的
        inserted = args.slice(2);
      default:
        break;
    }
    if (inserted) ob.observerArray(inserted); //将新增的数据，继续进行响应式处理
    ob.dep.notify(); // 如果触发数组的这7个方法，会进行页面更新操作
    return res;
  };
});
