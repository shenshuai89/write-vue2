import { initState } from "./state";
import { compileToFunction } from "./compiler/index";
import { mountComponent, callHook } from "./lifecycle";
import { mergeOptions } from "./util/index";
import { nextTick } from "./util/next-tick";
// 在Vue原型链上添加_init方法
export function initMixin(Vue) {
  Vue.prototype._init = function (options) {
    const vm = this;
    // vm.$options = options || {};
    vm.$options = mergeOptions(vm.constructor.options, options);

    // console.log("init11", vm.$options);

    // beforeCreate获取不到data中的数据
    callHook(vm, "beforeCreate");
    // 初始化状态
    initState(vm);

    // created可以获取到data中的数据
    callHook(vm, "created");

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
  Vue.prototype.$mount = function (el) {
    const vm = this;
    const options = vm.$options;
    el = document.querySelector(el);

    // 默认先查找使用render方法，没有则使用template编译，没有template最后才使用el中的内容
    if (!options.render) {
      // 对模板进行编译
      let template = options.template;
      if (!template && el) {
        template = el.outerHTML;
      }
      // console.log(template);
      // 接下来对template 转化为render函数
      const render = compileToFunction(template);
      options.render = render;

      // 渲染当前组件 并挂载组件
      mountComponent(vm, el);
    }
  };
  Vue.prototype.$nextTick = nextTick;
}
