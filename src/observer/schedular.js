import { nextTick } from "../util/next-tick";
let queue = [];
let has = {};
export function queueWatcher(watcher) {
  const id = watcher.id;
  if (has[id] == null) {
    queue.push(watcher);
    has[id] = true;
    // 这里使用nextTick对页面进行异步更新
    // 宏任务和微任务
    // Vue.nextTick 等价于 promise/ mutationObserver/ setImmediate/ setTimeout,进行降级
    nextTick(flushSchedularQueue)
  }
}

function flushSchedularQueue() {
  queue.forEach((watcher) => watcher.run());
  queue = [];
  has = {};
}
