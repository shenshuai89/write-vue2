import { parseHTML, defaultTagRE } from "./parser-html";
import { generate } from "./generate";
// 将template 转化为render
// ast语法树 用对象描述原生html语法 虚拟dom：用对象描述dom节点/结构
export function compileToFunction(template) {
  // @1: 解析html字符串， 将html字符串转ast语法树
  let root = parseHTML(template);
  // console.log(root); // root 即AST语法树
  // @2: 将ast语法树生成最终的render函数，即是字符串拼接的模板
  let code = generate(root);
  // console.log(code, "code****");
  // 模版引擎的实现 new Function + with
  let renderFn = new Function(`with(this){return ${code}}`);
  // console.log(renderFn); // 导出render生成的虚拟dom
  return renderFn;
}
