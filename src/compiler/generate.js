import { defaultTagRE } from "./parser-html";
export function generate(el) {
  const children = genChildren(el);

  let code = `_c("${el.tag}", ${
    el.attrs.length ? genProps(el.attrs) : "undefined"
  }${children ? `,${children}` : ""})`;
  return code;
}

function genProps(attrs) {
  let str = "";
  for (let i = 0; i < attrs.length; i++) {
    let attr = attrs[i];
    if (attr.name === "style") {
      let obj = {};
      attr.value.split(";").forEach((item) => {
        let [key, value] = item.split(":");
        obj[key] = value;
      });
      attr.value = obj;
    }
    str += `${attr.name}:${JSON.stringify(attr.value)}, `;
  }
  return `{${str.slice(0, -2)}}`;
}

function genChildren(el) {
  let children = el.children;
  if (children && children.length > 0) {
    return `${children.map((c) => gen(c)).join(", ")}`;
  } else {
    return false;
  }
}

function gen(node) {
  if (node.type === 1) {
    // 元素标签
    return generate(node);
  } else {
    let text = node.text; // 文本标签
    // _c("p", undefined,_v(hello{{age}})) 处理{{age}}差值对象_s(age)
    let tokens = [];
    let match, index;
    let lastIndex = (defaultTagRE.lastIndex = 0); // 只要是全局匹配g，需要将lastIndex每次匹配开始时重置为0
    while ((match = defaultTagRE.exec(text))) {
      index = match.index;
      if (index > lastIndex) {
        tokens.push(JSON.stringify(text.slice(lastIndex, index)));
      }
      tokens.push(`_s(${match[1].trim()})`);
      lastIndex = index + match[0].length;
    }
    if (lastIndex < text.length) {
      tokens.push(JSON.stringify(text.slice(lastIndex)));
    }

    return `_v(${tokens.join("+")})`; //经过处理后 _c("p", undefined,_v("hello"+_s(age)))
  }
}
