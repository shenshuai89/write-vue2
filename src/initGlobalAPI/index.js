import { initMixin } from "./mixin";
import { ASSETS_TYPE } from "./const";
import initAssetRegisters from "./assets";
import initExtend from "./extend"
export function initGlobalAPI(Vue) {
  Vue.options = {};
  initMixin(Vue);
  // 初始化全局过滤器 指令 组件
  ASSETS_TYPE.forEach((type) => {
    Vue.options[type + "s"] = {};
  });
  // _base 是Vue的构造函数
  Vue.options._base = Vue;

  // 注册Vue.extend方法
  initExtend(Vue);
  initAssetRegisters(Vue);


  // 初始化全局过滤器 指令 组件
  // Vue.options.filters = {};
  // Vue.options.directives = {};
  // Vue.options.components = {};

  /* Vue.mixin({
    a: 1,
    beforeCreate() {
      console.log("minx1");
    },
  });
  Vue.mixin({
    b: 2,
    beforeCreate() {
      console.log("minx2");
    },
  });
  // 通过mixin将传入的对象进行合并，生命周期通过不同的合并策略，组成数组
  console.log(Vue.options); */
}
