import { mergeOptions } from "../util/index";
export default function initExtend(Vue) {
  // 每个组件有个唯一标识cid
  let cid = 0;
  // extend返回一个子类
  Vue.extend = function (extendOptions) {
    // console.log(extendOptions);
    const Sub = function VueComponent(options) {
      this._init(options);
    };
    Sub.cid = cid++;
    // Sub 继承父类原型
    Sub.prototype = Object.create(this.prototype);
    // 重新设置Sub子类的构造函数
    Sub.prototype.constructor = Sub;
    Sub.options = mergeOptions(this.options, extendOptions);
    return Sub;
  };
}
