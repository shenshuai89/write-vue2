import { mergeOptions } from "../util/index";
export function initMixin(Vue) {
  //定义Vue对象下的mixin属性
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
  };
}
