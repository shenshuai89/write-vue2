## 手写vue2实现原理
### 初始化项目环境
``` bash
yarn add --dev rollup @babel/core @babel/preset-env rollup-plugin-babel rollup-plugin-serve cross-env
```
- rollup: 打包工具
- rollup-plugin-babel: rollup和babel建立连接的桥梁
- rollup-plugin-serve: rollup起静态服务
- babel/core: babel的核心模块
- babel/preset-env: babel的预置模块，将高级语法转为低级语法
- cross-env: 设置环境遍历

### rollup配置文件
创建rollup.config.js
``` javascript
input: "./src/index.js", //入口文件
output: {
file: "./dist/umd/vue.js", //出口路径
name: "Vue", // 打包后全局遍历的名字
format: "umd", //打包时统一模块规范
sourceMap: true, //开启源码调试，报错可以找到源码报错位置
},
```
### 添加babelrc配置文件
``` json
{
    "presets": [
        "@babel/preset-env"
    ]
}
```

## 配置package文件
### 设置不同启动命令
> 通过cross-env设置不同环境遍历
- build：进行编译创建
- serve：启动服务，在3000端口打开页面

## src/index.js项目入口
Vue的入口文件，当实例化Vue时进行初始化操作。内部调用各种Mixin方法，给Vue的原型上添加方法。
- initMixin增加初始化方法
- renderMixin增加渲染方法，调用render方法               ->  lifecycle.js
- lifecycleMixin增加update方法，将虚拟dom渲染成真实dom  ->  render.js
> 最核心的两个方法vm._update(vm._render())
### src/init.js
Vue初始化方法，包括后续组件的初始化，在这里初始化状态
针对用户传入的属性进行不同的初始化功能  -> state.js

### 其他目录
* compiler 实现将模板转换成ast树【编译流程】
* observer观察数据 响应式原理 【实现对象和数组的数据劫持】
* vdom 创建虚拟节点和将虚拟节点渲染成真实节点【熟悉虚拟节点和渲染流程】
### render渲染流程
#### 初始化渲染流程
1. 从init文件中开始，调用mountComponent函数，通过生命周期函数挂载组件。
2. mountComponent函数中包含
 - updateComponent函数，该函数内包含update和render
 - new Watcher创建watcher，通过第二个参数，将updateComponent传递到Watcher类中。
 - 在类内部调用updateComponent函数执行
3. _update方法在index文件的initLifeCycle方法调用
4. _render方法在index文件的initRender方法中调用

## Mixin合并属性
为了把Vue内部的属性和用户传递的属性options合并，使用了全局方法mixin。
mixin使用mergeOptions方法。
import { mergeOptions } from "../util/index";

### mergeOptions方法
方法分为合并属性和合并生命周期不同方案策略。
#### 属性使用mergeField
``` javascript
function mergeField(key) {
    if (strats[key]) {
        return (options[key] = strats[key](parent[key], child[key]));
    }
    // parent child all object
    if (typeof parent[key] === "object" && typeof child[key] === "object") {
        options[key] = {
        ...parent[key],
        ...child[key],
        };
    } else if (child[key] == null) {
        options[key] = parent[key];
    } else {
        options[key] = child[key];
    }
}
```
#### 生命周期使用mergeHook
``` javascript
function mergeHook(parentVal, childVal) {
  if (childVal) {
    if (parentVal) {
      return parentVal.concat(childVal);
    } else {
      return [childVal];
    }
  } else {
    return parentVal;
  }
}
```
## 数组的依赖收集
在Observer类中创建Dep实例，以供数组使用。
在defineReactive方法中，单独处理数组类型的数据
``` javascript
if (childObj) {
  // *******数组的依赖收集********
  childObj.dep.depend();
  // 如果数组中还是数组
  if (Array.isArray(value)) {
    // 把内部数组的每一个数组都进行依赖收集
    dependArray(value);
  }
}
```
## 异步更新 使用nextTick
promise/ mutationObserver/ setImmediate/ setTimeout,使用方法依次进行降级