import babel from "rollup-plugin-babel";
import serve from "rollup-plugin-serve";
const isDev = process.env.NODE_ENV === "development";
export default {
  input: "./src/index.js", //入口文件
  output: {
    file: "./dist/umd/vue.js", //出口路径
    name: "Vue", // 打包后全局遍历的名字
    format: "umd", //打包时统一模块规范
    sourceMap: true, //开启源码调试，报错可以找到源码报错位置
  },
  plugin: [
    babel({
      exclude: "node_modules", // 排除的目录
    }),
    isDev
      ? serve({
          open: true,
          openPage: "/public/index.html",
          port: 3000,
          contentBase: "",
        })
      : null,
  ],
};
